package com.teste.maurosantos.testeandroid;


import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.io.InputStream;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void botaoProgramacao(View view){
        Intent intent = new Intent(this, Programacao.class);
        startActivity(intent);
    }

    public void botaoManhaTrackA(View view){
        Intent intent = new Intent(this, ManhaTrackA.class);
        startActivity(intent);
    }

    public void botaoManhaTrackB(View view){
        Intent intent = new Intent(this, ManhaTrackB.class);
        startActivity(intent);
    }

    public void botaoTardeTrackA(View view){
        Intent intent = new Intent(this, TardeTrackA.class);
        startActivity(intent);
    }

    public void botaoTardeTrackB(View view){
        Intent intent = new Intent(this, TardeTrackB.class);
        startActivity(intent);
    }

}
