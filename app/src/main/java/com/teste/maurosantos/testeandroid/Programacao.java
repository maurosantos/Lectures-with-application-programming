package com.teste.maurosantos.testeandroid;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.io.InputStream;

public class Programacao extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programacao);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView txtFileName = (TextView) findViewById(R.id.id_programacao);

        AssetManager assetManager = getAssets();

        InputStream input;

        try {

            input=assetManager.open("proposals.txt");

            int size=input.available();

            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();

            String text = new String(buffer);

            txtFileName.setText(text);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
